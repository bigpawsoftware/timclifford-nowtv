import React from 'react';
import ReactDOM from 'react-dom';

import Message from './Message';

const messages = [
  {
    messageId: '12356',
    userId: '613651251',
    fullName: 'Robin Balmforth',
    timestamp: '2017-02-23T14:57:20.629Z',
    email: 'robin@example.com',
    message: 'Hello, World!',
    avatar: null,
  },
  {
    messageId: '12357',
    userId: '613651251',
    fullName: 'Robin Balmforth',
    timestamp: '2017-02-24T14:57:20.629Z',
    email: 'robin@example.com',
    message: 'Hello, World 2!',
    avatar: 'http://whatever.com/a_picture.png',
  },
];

it('renders without crashing where there is no avatar', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Message message={messages[0]} />, div);
});

it('renders without crashing where there is an avatar', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Message message={messages[1]} />, div);
});
