import React from 'react';
import PropTypes from 'prop-types';

const Message = ({ message }) => (
  <li>
    <article className="message" title={message.email}>
      <div className="message__avatar-container">
        {message.avatar ? <img src={message.avatar} alt={`Avatar for ${message.fullName}`} className="message__avatar" /> : null}
      </div>

      <div className="message__content">
        <p>{message.message} <span className="id">({message.messageId})</span></p>

        <div className="message__content-info">
          <p>{message.fullName} <span className="id">({message.userId})</span></p>
          <p>Posted on {new Date(message.timestamp).toUTCString()}</p>
        </div>
      </div>
    </article>
  </li>
);

Message.propTypes = {
  message: PropTypes.shape({
    messageId: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    fullName: PropTypes.string.isRequired,
    timestamp: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    avatar: PropTypes.string,
  }).isRequired
};

export default Message;
