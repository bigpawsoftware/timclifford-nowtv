import React from 'react';
import ReactDOM from 'react-dom';

import MessageList from './MessageList';

const messages = [
  {
    messageId: '12356',
    userId: '613651251',
    fullName: 'Robin Balmforth',
    timestamp: '2017-02-23T14:57:20.629Z',
    email: 'robin@example.com',
    message: 'Hello, World!',
    avatar: null,
  },
  {
    messageId: '12357',
    userId: '613651251',
    fullName: 'Robin Balmforth',
    timestamp: '2017-02-24T14:57:20.629Z',
    email: 'robin@example.com',
    message: 'Hello, World 2!',
    avatar: 'http://whatever.com/a_picture.png',
  },
];

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MessageList messages={messages} />, div);
});
