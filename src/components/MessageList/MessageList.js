import React from 'react';
import PropTypes from 'prop-types';

import Message from '../Message/Message';

const MessageList = ({ messages }) => (
  <ul className="message-list">
    {messages.map(message => (
      <Message key={message.messageId} message={message} />
    ))}
  </ul>
);

MessageList.propTypes = {
  messages: PropTypes.arrayOf(
    PropTypes.shape({
      messageId: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired,
      timestamp: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired,
      avatar: PropTypes.string,
    }).isRequired
  ).isRequired,
};

export default MessageList;
