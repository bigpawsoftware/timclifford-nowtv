import { getMembers, getMessages } from './data';

const membersMap = new Map();
let unsortedMessages = [];

async function getChatLog(sortField = 'timestamp', useCache = true) {
  await getMembersAsMap(useCache);
  await getUnsortedMessages(useCache);
  // return unsortedMessages.sort((a, b) => a[sortField] > b[sortField] ? 1 : -1);
  return mergeSort(unsortedMessages, sortField);
}

async function getMembersAsMap(useCache) {
  if (!useCache) {
    membersMap.clear();
  }

  if (membersMap.size === 0) {
    const members = await getMembers();

    members.forEach(({ id, ...member }) => {
      membersMap.set(id, member);
    });
  }
}

async function getUnsortedMessages(useCache) {
  if (!useCache) {
    unsortedMessages.length = 0;
  }

  if (unsortedMessages.length === 0) {
    unsortedMessages = await getMessages();
    mergeMemberDataIntoMessages(unsortedMessages, membersMap);
  }
}

function mergeArrays(left, right, sortField) {
  const mergedArray = [];
  let leftIndex = 0;
  let rightIndex = 0;

  while (leftIndex < left.length && rightIndex < right.length) {
    if (left[leftIndex][sortField] < right[rightIndex][sortField]) {
      mergedArray.push(left[leftIndex]);
      leftIndex++;
    } else {
      mergedArray.push(right[rightIndex]);
      rightIndex++;
    }
  }

  mergedArray.push(...left.slice(leftIndex), ...right.slice(rightIndex));

  return mergedArray;
}

function mergeMemberDataIntoMessages(messages, membersMap) {
  messages.forEach(msg => {
    const member = membersMap.get(msg.userId);

    msg.messageId = msg.id;
    delete msg.id;

    msg.avatar = member.avatar;
    msg.email = member.email;
    msg.fullName = `${member.firstName} ${member.lastName}`;
  });
}

function mergeSort(unsortedArray, sortField) {
  if (unsortedArray.length < 2) {
    return unsortedArray;
  }

  const middle = Math.floor(unsortedArray.length / 2);
  const left = unsortedArray.slice(0, middle);
  const right = unsortedArray.slice(middle);

  return mergeArrays(mergeSort(left, sortField), mergeSort(right, sortField), sortField);
}

export default getChatLog;
