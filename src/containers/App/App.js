import React, { Component } from 'react';

import getChatLog from '../../service';

import MessageList from '../../components/MessageList/MessageList';

class App extends Component {
  state = {
    messages: [],
  };

  componentDidMount() {
    getChatLog() // Can optionally specify the desired sort field here, and whether caching should be used
      .then(messages => {
        this.setState({ messages });
      })
      .catch(error => {
        console.log('Something went wrong!', error.message);
      });
  }

  render() {
    return (
      <main>
        <h1 className="title">Chat Log</h1>

        <MessageList messages={this.state.messages} />
      </main>
    );
  }
}

export default App;
